<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ContactTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * Test that you can create a contact
     *
     * @return void
     */
    public function testCreateContact()
    {
        $response = $this->post(
            '/api/login',
            [
            "email" => "c.ryan@mun.ca",
            "password" => "catsass",
            ]
        );


        $token = $response->original['success']['token'];
        $response2 = $this->withHeaders(
            [
                'Authorization' => "Bearer $token",
                'Content-Type' => 'Application/Json'
            ]
        )->post(
            '/api/users/1/contacts',
            [
                "name" => "cats",
                "phoneNumber" => "7081234567",
                ]
        );

        $response2->assertStatus(201);
    }
}
