<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthTest extends TestCase
{
    /**
     * Tests registration
     *
     * @return void
     */
    public function testRegister()
    {
        $response = $this->post(
            '/api/register',
            [
            "name" => "Colin",
            "email" => "test@test.com",
            "password" => "catsass",
            "c_password" => "catsass"

            ]
        );

        $response->assertStatus(200);
    }

    /**
     *  tests login
     *
     * @return void
     */
    public function testLogin()
    {
        $response = $this->post(
            '/api/login',
            [
            "email" => "test@test.com",
            "password" => "catsass",
            ]
        );

        $response->assertStatus(200);
    }
    
    /**
     * deletes a user based on email
     *
     * @return void
     */
    public function testDeleteUser()
    {
        $response = $this->post(
            '/api/login',
            [
            "email" => "test@test.com",
            "password" => "catsass",
            ]
        );

        $token = $response->original['success']['token'];

        $response2 = $this->withHeaders(
            [
            'Authorization' => "Bearer $token"
            ]
        )->json('delete', '/api/users');

        $response2->assertStatus(200);
    }
}
