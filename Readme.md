# Setup


Make sure you have php-sqlite installed and uncomment
extension=pdo_sqlite
extension=sqlite3

in your php.ini


Make sure you have sqlite3 installed on your machine.


run chmod +x on setup.sh


To use the api, first register, then login. You'll get a token from the login
that you'll have to use in any contact based request. Routes look like this:

api/register
api/login

Both are posts and both expect email and password. Register needs name and
c_password fields as well. Everything sent as JSON.

The contact routes typical crud routes. they have the following root

/api/users/{userId}/contacts


Post to it to create, add a contactsId to the end and get to show a contact,
delete to delete the contact, and put/patch to update the contact.
