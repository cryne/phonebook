#! /usr/bin/env bash



composer install
touch database/database.sqlite
php artisan migrate
php artisan passport:keys
php artisan passport:client --personal # (I answeredpac)
php artisan db:seed
./vendor/bin/phpunit
