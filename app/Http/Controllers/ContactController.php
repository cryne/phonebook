<?php

namespace App\Http\Controllers;

use App\User;
use App\Contact;
use App\Http\Resources\ContactResource;
use App\Http\Resources\ContactCollection;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        return new ContactCollection($user->contacts());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(User $user, Request $request)
    {
        $contact = $user->contacts()->create($request->all());
        return new ContactResource($contact);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contact $contact
     * @return \Illuminate\Http\Response
     */
    public function show(User $user, Contact $contact)
    {
        $con = $user->contacts()->find($contact->id);
        return new ContactResource($con);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Contact             $contact
     * @return \Illuminate\Http\Response
     */
    public function update(User $user, Request $request, Contact $contact)
    {
        $con = $user->contacts()->find($contact->id);
        $con->update($request->all());
        
        return new ContactResource($con);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, Contact $contact)
    {
        $con = $user->contacts()->find($contact->id);
        $con->delete();
        return new ContactResource($con);
    }
}
